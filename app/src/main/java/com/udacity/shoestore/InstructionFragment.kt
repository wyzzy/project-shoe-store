package com.udacity.shoestore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.udacity.shoestore.databinding.InstructionsBinding
import com.udacity.shoestore.databinding.WelcomeBinding

class InstructionFragment : Fragment() {
  private lateinit var binding: InstructionsBinding

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    binding = DataBindingUtil.inflate(
      inflater,
      R.layout.instructions,
      container,
      false
    )

    binding.nextButton.setOnClickListener {
      findNavController().navigate(InstructionFragmentDirections.actionInstructionsFragmentToShoeListFragment())
    }

    return binding.root
  }

}