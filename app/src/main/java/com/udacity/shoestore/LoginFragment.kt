package com.udacity.shoestore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.udacity.shoestore.databinding.LoginBinding
import com.udacity.shoestore.databinding.WelcomeBinding
import timber.log.Timber

class LoginFragment : Fragment() {
  private lateinit var binding: LoginBinding

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    binding = DataBindingUtil.inflate(
      inflater,
      R.layout.login,
      container,
      false
    )

    binding.loginButton.setOnClickListener {
      performLogin()
    }

    binding.signupButton.setOnClickListener {
      performLogin()
    }

    return binding.root
  }

  fun performLogin(){
    var hasError = false
    if(binding.emailInput.text.toString() == ""){
      hasError = true
      binding.emailInput.error = "Email is required"
    }

    if(binding.passwordInput.text.toString() == ""){
      hasError = true
      binding.passwordInput.error = "Password is required"
    }
    if (!hasError){
      findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToWelcomeFragment())
    }
  }

}