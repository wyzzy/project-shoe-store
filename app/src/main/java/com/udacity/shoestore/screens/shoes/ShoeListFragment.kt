package com.udacity.shoestore.screens.shoes

import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.ShoeItemBinding
import com.udacity.shoestore.databinding.ShoeListBinding
import timber.log.Timber


class ShoeListFragment : Fragment() {
    private val shoesViewModel: ShoesViewModel by activityViewModels()
    private lateinit var binding: ShoeListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
            R.layout.shoe_list,
                container,
                false
        )
        val shoesLayout = binding.root.findViewById<LinearLayout>(R.id.shoeListLayout)

        shoesViewModel.shoeList.observe(viewLifecycleOwner, Observer { shoeList ->
            shoeList.forEach{ shoe ->
                val shoeItemBinding = DataBindingUtil.inflate<ShoeItemBinding>(inflater, R.layout.shoe_item, container, false)
                shoeItemBinding.shoe = shoe
                shoesLayout.addView(shoeItemBinding.root)
            }
        })

        binding.newShoeButton.setOnClickListener {
            findNavController().navigate(ShoeListFragmentDirections.actionListToAddShoe())
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }
}