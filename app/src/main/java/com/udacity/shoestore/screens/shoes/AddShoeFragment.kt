package com.udacity.shoestore.screens.shoes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.udacity.shoestore.screens.shoes.AddShoeFragmentDirections
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.AddShoeBinding
import com.udacity.shoestore.models.Shoe

class AddShoeFragment : Fragment() {
    private val shoesViewModel: ShoesViewModel by activityViewModels()
    private lateinit var binding: AddShoeBinding

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.add_shoe,
            container,
            false
        )
        binding.shoesViewModel = shoesViewModel
        binding.shoe = shoesViewModel.emptyShoe()
        binding.lifecycleOwner = this

        binding.cancelButton.setOnClickListener {
            findNavController().navigate(AddShoeFragmentDirections.actionAddShoeFragmentToShoeListFragment())
        }

        shoesViewModel.shoeAdded.observe(viewLifecycleOwner, Observer { shoeAdded ->
            if (shoeAdded){
                findNavController().navigate(AddShoeFragmentDirections.actionAddShoeFragmentToShoeListFragment())
                shoesViewModel.shoeAddedFinish()
            }
        })

        return binding.root
    }
}