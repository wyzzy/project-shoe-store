package com.udacity.shoestore.screens.shoes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.models.Shoe
import timber.log.Timber

class ShoesViewModel : ViewModel() {

    private val _shoeList = MutableLiveData<MutableList<Shoe>>()
    val shoeList: LiveData<MutableList<Shoe>>
        get() = _shoeList

    private val _shoeAdded = MutableLiveData<Boolean>()
    val shoeAdded: LiveData<Boolean>
        get() = _shoeAdded

    init {
        val sampleShoe = Shoe("Name", 47.5, "Company", "Description")
        val sampleShoe2 = Shoe("Name2", 41.5, "Company2", "Description2")
        _shoeList.value = mutableListOf()
        _shoeList.value!!.add(sampleShoe)
        _shoeList.value!!.add(sampleShoe2)
    }

    fun emptyShoe(): Shoe{
        return Shoe("", 0.0, "", "")
    }

    fun addShoe(shoe: Shoe){
        _shoeList.value!!.add(shoe)
        _shoeAdded.value = true
    }

    fun shoeAddedFinish(){
        _shoeAdded.value = false
    }
}